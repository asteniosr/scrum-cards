angular.module('app', []).

  //definimos las rutas de la 'app'
  config(['$routeProvider', function($routes) {
  
  $routes.
      when('/issues', {
		  templateUrl: 'src/views/issues.html',
		  controller: IssuesListController
		  }).
	  
	  //cualquier ruta no definida  
      otherwise({
		  redirectTo: '/issues'});

}]);