/**
* IssuesListController
*/
function IssuesListController($scope, $http) {

  $url = 'data/issues.json';
  $http({method: 'GET', url: $url}).
	success(function(data, status) {
	  $scope.status = status;
	  $scope.data = data;
	}).
	error(function(data, status) {
	  $scope.data = data || "Request failed";
	  $scope.status = status;
	});
 
}